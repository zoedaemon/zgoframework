// zgoframework project main.go
package main

import (
	"zgof"

	//NOTE jika menggunakan tanda "_" di  _ "zgoframework/router"
	//		pemanggilan Object &router.RouterAPI{} bisa gagal

	"zgoframework/router"
)

func main() {

	ZG := zgof.New()

	//deklarasi multi router object
	//NOTE : tanda &Object{} adalah init NULL (tanda {}) untuk object Object dan passing
	//			passbyreference k dalam fungsi (tanda &)
	ZG.RegisterRouters(&router.RouterAPI{})
	ZG.RegisterRouters(&router.RouterHtml{})

	//set path k static page
	asset := "D:\\roy\\STARTUP\\golang\\GOPATH\\src\\kointour\\frontend\\content\\asset\\web"
	ZG.RegisterStatic("/web/*filepath", asset)

	asset = "D:\\roy\\STARTUP\\golang\\GOPATH\\src\\kointour\\frontend\\content\\sharing_asset"
	ZG.RegisterStatic("/asset/*filepath", asset)

	//TODO : redis cache cuy biar cepat........
	dir := "D:\\roy\\STARTUP\\golang\\GOPATH\\src\\kointour\\frontend\\content\\pages\\web\\index.html"

	ZG.CompileView(dir)
	//	ZG.CompileViewFile(file)

	//_ = router.Index{}//di import bisa gunakan tanda _

	//Penggantinya
	//	ZG.AutoRouter("GET", router.Simple("/"), view.Html("index.html", "utf-8"), Index)
	//	ZG.AutoRouter("GET", router.Wildcard("/static/*"), view.StaticFile("statics/*", "auto"), nil)

	//host disetting k localhost dan 127.0.0.1 lelet gila.. =))
	//bagusnya kosongkan ato pakai IP fisik ato IP DHCP
	ZG.Host = "" //"192.168.43.19" contohnya bisa pake IP DHCP

	//ZO.Listen(_, 8080)//gak bisa parameter optional :(
	//ZG.Listen(8080, _)
	ZG.Listen(8080)
}
