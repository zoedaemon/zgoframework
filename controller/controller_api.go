package controller

import (
	"fmt"
	"net/http"

	"zgof"

	"github.com/julienschmidt/httprouter"
)

type IndexAPI struct {

	//Tuk Interfacenya gimana....?
	//zgof.ControllerInterface

	//NOTE : otomatis diinisialisasikan oleh RegisterRouters
	zgof.Controller
}

type Title struct {
	Caption string
}

func (c *IndexAPI) Get(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {

	var title string
	//	fmt.Fprintln(w, "Controllernya berhasil..............")
	//c.ZgofPtr.ResponseTest(w, "object pointer berhasil :P")

	view := c.ZgofPtr.View
	session := c.ZgofPtr.Session

	//Test Session
	sess, _ := session.SessionStart(w, r)
	defer sess.SessionRelease(w)
	username := sess.Get("username")
	if username != nil {
		title = "Selamat Datang : " + username.(string) + "... :D"
		//Test Langsung hapus
		sess.Delete("username")
	} else {
		sess.Set("username", "Zoe Daemon")
		title = "Ooopppsss username baru di-set "
	}

	//TODO: perlu "defer" object buatan ???
	caption := Title{title}
	//TODO: perlu Context atau sharing antar middleware ???
	//TODO: current router juga perlu diinisialisaikan...??
	w.Header().Set("content-type", "text/html;charset=utf-8")
	//NOTE: Harus panggil CompileView terlebih dahulu
	view.Execute(w, caption)

}

func (c *IndexAPI) NotDefaultFunction() {
	//	c.Data["Website"] = "beego.me"
	//	c.Data["Email"] = "astaxie@gmail.com"
	//	c.TplNames = "index.tpl"
}

func (c *IndexAPI) Patch(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	fmt.Fprintln(w, "Patch berhasil..............")
}
