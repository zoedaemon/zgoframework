package router

import (
	"fmt"
	"net/http"

	"zgof"
	//_ "zgof/Router"

	"zgoframework/controller"

	"github.com/julienschmidt/httprouter"
)

type RouterAPI struct {
	zgof.Router //TODO: ganti router object bukan interface
}

func Index(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	fmt.Fprintln(w, "Hello zgof")
}

func TravelPackage(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	fmt.Fprintln(w, "Hello zgof 2")
}

func (r *RouterAPI) IndexRoutes(zg *zgof.Zgof) {

	/*
		IndexRoutes diparser jd Index saja dan otomatis menunjuk
		r.AutoRouter("GET", router.Simple("/"), viewFormat.Html("index.html", "utf-8"),
					&controller.Index{})
	*/
	//r.AutoRouter("GET", routerPath.Simple("/"), viewFormat.Html("index.html", "utf-8"))
	//r.AutoRouter("GET", routerPath.Simple("/json"), viewFormat.Json("utf-8"))

	//	zg.Router("GET", "/", Index)

	//NOTE: gak perlu router manual lg coz langsung panggil dr controller
	zg.AutoRouter("/", &controller.IndexAPI{})

	//	return r
}

func (r *RouterAPI) TravelPackageRoutes(zg *zgof.Zgof) {

	/*
		IndexRoutes diparser jd Index saja dan otomatis menunjuk
		r.AutoRouter("GET", routerPath.Simple("/"), viewFormat.Html("index.html", "utf-8"),
					&controller.TravelPackage{})
	*/
	//r.AutoRouter("GET", routerPath.Simple("/"), viewFormat.Html("index.html", "utf-8"))
	//r.AutoRouter("GET", routerPath.Simple("/json"), viewFormat.Json("utf-8"))

	zg.Router("GET", "/app/:session/travel-package/:id", TravelPackage)

	//	return r
}

//NOTE : gak perlu pake fungsi init :P
func init() {
	//beego.Router("/", &controller.MainController{})
	//fmt.Println("Register Routers...", &controller.MainController{})
}
