package router

import (
	"zgof"
	//_ "zgof/Router"

	//	"zgoframework/controller"

	//	"github.com/julienschmidt/httprouter"
)

type RouterHtml struct {
	zgof.Router
}

func (r *RouterAPI) BeforeAutoExec(zg *zgof.Zgof, user_data interface{}) {

}

func (r *RouterHtml) IndexHtlmRoutes(zg *zgof.Zgof) {
	/*
		IndexRoutes diparser jd Index saja dan otomatis menunjuk
		r.AutoRouter("GET", router.Simple("/"), viewFormat.Html("index.html", "utf-8"),
					&controller.Index{})
	*/
	//r.AutoRouter("GET", routerPath.Simple("/"), viewFormat.Html("index.html", "utf-8"))
	//r.AutoRouter("GET", routerPath.Simple("/json"), viewFormat.Json("utf-8"))

	zg.Router("GET", "/html", Index)

	//	return r
}

func (r *RouterHtml) TravelPackageHtlmRoutes(zg *zgof.Zgof) {

	/*
		IndexRoutes diparser jd Index saja dan otomatis menunjuk
		r.AutoRouter("GET", routerPath.Simple("/"), viewFormat.Html("index.html", "utf-8"),
					&controller.TravelPackage{})
	*/
	//r.AutoRouter("GET", routerPath.Simple("/"), viewFormat.Html("index.html", "utf-8"))
	//r.AutoRouter("GET", routerPath.Simple("/json"), viewFormat.Json("utf-8"))

	zg.Router("GET", "/app/:session/travel-package/:id/html", TravelPackage)

	//	return r
}

func init() {
	//beego.Router("/", &controller.MainController{})
	//fmt.Println("Register Routers...", &controller.MainController{})
}
